# Project 6: Brevet time calculator service

Author: Lucas Hyatt

Simple listing service from project 5 stored in MongoDB database.

## About this project

This project expands further on the Brevet calculator by adding RESTful service functionality. This service allows user to expose what is contained with the MongoDB module. Users can now access numerous URL's from the local host to highlight database contents in both JSON and csv format.

Basic API's: 

- http://<host:port>/listAll" should return all open and close times in the database
- http://<host:port>/listOpenOnly" should return open times only
- http://<host:port>/listCloseOnly" should return close times only

Ways to Represent CSV vs. JSON:

- "http://<host:port>/listAll/csv" should return all open and close times in CSV format
- "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
- "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
- "http://<host:port>/listAll/json" should return all open and close times in JSON format
- "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
- "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

Query Parameters (only applicable to listOpenOnly and listCloseOnly):

- http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format
- http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
- http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
- http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

Additionally:

- Basic API functionality resides on port 5001, and can be accessed using the above URI's
- The previous brevet calculator still resides on port 5000. 
- Consumer program is implemented using HTML and PHP and can be accessed using the port 5002.

Error Checking:

- Error checking is implemented throughout, and will check that the database has contents before displaying any values. 
- Query parameters are error checked to ensure they are positive values and are not larger than the contents of the times (i.e. showing top 10 open times when there are only 8 in the database)
- If a query time is greater than available times, all times are shown. It also supports showing top 0 times (no times).

For further questions, email llh@uoregon.edu



