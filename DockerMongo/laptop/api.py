# Laptop Service

from flask import Flask
from flask import request
from flask_restful import Resource, Api
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)

from pymongo import MongoClient

# print(os.environ)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###################### Standard Representations (JSON) ######################

class allTimes(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]
		print(items)

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		return {
		    'Closed': items[0]['description'],
		    'Open': items[0]['name']
		}

api.add_resource(allTimes, '/listAll')

class openTimes(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]
		print(items)

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		return {
		    'Open': items[0]['name']
		}

api.add_resource(openTimes, '/listOpenOnly')

class closeTimes(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]
		print(items)

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		return {
			'Closed': items[0]['description'] 
		}

api.add_resource(closeTimes, '/listCloseOnly')


###################### JSON representations ######################

class allTimesJSON(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		return {
		    'Closed': items[0]['description'],
			'Open': items[0]['name']
		}

api.add_resource(allTimesJSON, '/listAll/json')

class openTimesJSON(Resource):
	def get(self):

		_items = db.tododb.find()

		items = [item for item in _items]
		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		# Check for query parameter
		if request.query_string:
			val = int(request.query_string[-1]) - 48
			if val > -1 and val < len(items[0]['name']):
				new_ret = list()
				for i in range(val):
					new_ret.append(items[0]['name'][i])

				return {
				    'Open': new_ret
				}

		return {
		    'Open': items[0]['name']
		}

api.add_resource(openTimesJSON, '/listOpenOnly/json')

class closeTimesJSON(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		# Check for query parameter
		if request.query_string:
			val = int(request.query_string[-1]) - 48
			if val > -1 and val < len(items[0]['description']):
				new_ret = list()
				for i in range(val):
					new_ret.append(items[0]['description'][i])

				return {
				    'Closed': new_ret
				}

		return {
			'Closed': items[0]['description'] 
		}

api.add_resource(closeTimesJSON, '/listCloseOnly/json')


###################### CSV representations ######################

class allTimesCSV(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		ret_str = ""
		ret_str += "Open Times, "

		for elem in items[0]['name']:
			ret_str += elem
			if elem != items[0]['name'][len(items[0]['name'])-1]:
				ret_str += ", "

		ret_str += ", Close Times, "
		for elem in items[0]['description']:
			ret_str += elem
			if elem != items[0]['description'][len(items[0]['description'])-1]:
				ret_str += ", "

		return ret_str

api.add_resource(allTimesCSV, '/listAll/csv')

class openTimesCSV(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]

		if items == []:
			return "Database Empty. Go back to calc and enter control points."

		new_ret = items[0]['name']
		# Check for query parameter
		if request.query_string:
			val = int(request.query_string[-1]) - 48
			if val > -1 and val < len(items[0]['name']):
				new_ret = list()
				for i in range(val):
					new_ret.append(items[0]['name'][i])

		ret_str = "Open Times, "
		for elem in new_ret:
			ret_str += elem
			if elem != new_ret[len(new_ret)-1]:
				ret_str += ", "

		return ret_str

api.add_resource(openTimesCSV, '/listOpenOnly/csv')

class closeTimesCSV(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]

		if items == []:
			return "Database Empty. Go back to calc and enter control points."
			
		new_ret = items[0]['description']
		# Check for query parameter
		if request.query_string:
			val = int(request.query_string[-1]) - 48
			if val > -1 and val < len(items[0]['description']):
				new_ret = list()
				for i in range(val):
					new_ret.append(items[0]['description'][i])

		ret_str = "Close Times, "
		for elem in new_ret:
			ret_str += elem
			if elem != new_ret[len(new_ret)-1]:
				ret_str += ", "

		return ret_str

api.add_resource(closeTimesCSV, '/listCloseOnly/csv')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)






