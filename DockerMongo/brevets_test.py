"""
Nose tests for flask_brevet.py

Essentially testing that the correct ISO 8601 format is returned when a
brevent control distance is inputted.
"""

import acp_times
import nose
import arrow


# Tests for the open_time method. Verifies correct ISO format 

DATE = arrow.get("2017-01-01T00:00:00+00:00").isoformat()

def test_open60km():
	assert acp_times.open_time(60, 200, DATE) == arrow.get("2017-01-01T01:46:00+00:00").isoformat()

def test_open350km():
	assert acp_times.open_time(350, 600, DATE) == arrow.get("2017-01-01T10:34:00+00:00").isoformat()

def test_open550km():
	assert acp_times.open_time(550, 600, DATE) == arrow.get("2017-01-01T17:08:00+00:00").isoformat()


# Tests for the close_time method. Verifies correct ISO format

def test_close60km():
	assert acp_times.close_time(60, 200, DATE) == arrow.get("2017-01-01T04:00:00+00:00").isoformat()

def test_close350km():
	assert acp_times.close_time(350, 600, DATE) == arrow.get("2017-01-01T23:20:00+00:00").isoformat()

def test_close550km():
	assert acp_times.close_time(550, 600, DATE) == arrow.get("2017-01-02T12:40:00+00:00").isoformat()