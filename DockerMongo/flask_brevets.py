"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import os
from pymongo import MongoClient

# print(os.environ)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    date = request.args.get('date', type=str)
    iso_date = arrow.get(date).isoformat()
    print()
    print("Retrieved date as", date)
    print("ISOFORMAT", arrow.get(date).isoformat())
    print()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, 200, iso_date)
    close_time = acp_times.close_time(km, 200, iso_date)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

# Request handler for submitting to the database.
@app.route('/_new')
def new():

    db.tododb.remove({})
    
    open_times = request.args.getlist("open")
    close_times = request.args.getlist("close")

    # Test case #1
    if open_times[0] == "":
        print()
        raise ValueError("There were no control times entered!")
        print()
        return flask.redirect(flask.url_for('index'))

    final_open = list()
    final_closed = list()

    # Inserting into a list of open_times and close_times, respectively
    for i in range(0, 20):
        if open_times[i] != "":
            final_open.append(open_times[i])
            final_closed.append(close_times[i])
        else:
            break
    print("Returned table:", final_open)
    print("Returned table:", final_closed)

    item_doc = {
        'name': final_open,
        'description': final_closed
    }
    db.tododb.insert_one(item_doc)

    return flask.redirect(flask.url_for('index'))

# Request handler for displaying the database 
@app.route('/display', methods=['POST'])
def todo():
    _items = db.tododb.find()

    items = [item for item in _items]
    print(items)

    return flask.render_template('display.html', items=items)

    

app.debug = CONFIG.DEBUG
# app.run(debug=True)

if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
