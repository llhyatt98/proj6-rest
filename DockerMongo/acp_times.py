"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# Control location (km)     Minimum Speed (km/hr)     Maximum Speed (km/hr)
# 0 - 200                   15                        34
# 200 - 400                 15                        32
# 400 - 600                 15                        30
# 600 - 1000                11.428                    28
# 1000 - 1300               13.333                    26

def control_time(time):
    '''
    Helper function which calculates the time given in total hours.
    '''
    hours = math.floor(time)
    minutes = round((time - hours)*60)
    return (hours, minutes)


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km <= 200:
      input_hrs = control_dist_km/34
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 200 and control_dist_km <= 400:
      input_hrs = (200/34) + ((control_dist_km-200)/32)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 400 and control_dist_km <= 600:
      input_hrs = (200/34) + (200/32) + ((control_dist_km-400)/30)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 600 and control_dist_km <= 1000:
      input_hrs = (200/34) + (200/32) + (200/30) + ((control_dist_km-600)/28)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 1000 and control_dist_km <= 1300:
      input_hrs = (200/34) + (200/32) + (200/30) + (400/28) + ((control_dist_km-1000)/26)
      time = control_time(input_hrs)
      # print(time)
    else:
      print("Control distance not between 0 and 1300")
    
    # print("Current time is:", arrow.get(brevet_start_time))
    # print(time[0], time[1])
    # print("Updated time is:", arrow.get(brevet_start_time).shift(hours=time[0], minutes=time[1]).isoformat())
    brev_start = arrow.get(brevet_start_time)
    return brev_start.shift(hours=time[0], minutes=time[1]).isoformat()


# print("Open test cases:")
# Example 1
# print(open_time(550, 200, arrow.get("2017-01-01T00:00:00+00:00").isoformat()))
# open_time(120, 200, arrow.now().isoformat())
# open_time(175, 200, arrow.now().isoformat())
# open_time(200, 200, arrow.now().isoformat())

# Example 2
# open_time(350, 600, arrow.now().isoformat())
# open_time(550, 600, arrow.now().isoformat())
# open_time(890, 600, arrow.now().isoformat())
# open_time(1200, 600, arrow.now().isoformat())




def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km <= 200:
      input_hrs = control_dist_km/15
      time = control_time(input_hrs)
      # print("Time is:", time)
    elif control_dist_km > 200 and control_dist_km <= 400:
      input_hrs = (200/15) + ((control_dist_km-200)/15)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 400 and control_dist_km <= 600:
      input_hrs = (200/15) + (200/15) + ((control_dist_km-400)/15)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 600 and control_dist_km <= 1000:
      input_hrs = (200/15) + (200/15) + (200/15) + ((control_dist_km-600)/11.428)
      time = control_time(input_hrs)
      # print(time)
    elif control_dist_km > 1000 and control_dist_km <= 1300:
      input_hrs = (200/15) + (200/15) + (200/15) + (400/11.428) + ((control_dist_km-1000)/13.333)
      time = control_time(input_hrs)
      # print(time)
    else:
      print("Control distance not between 0 and 1300")

    # print("Current time is:", arrow.get(brevet_start_time))
    # print(time[0], time[1])
    # print("Updated time is:", arrow.get(brevet_start_time).shift(hours=time[0], minutes=time[1]).isoformat())
    brev_start = arrow.get(brevet_start_time)
    return brev_start.shift(hours=time[0], minutes=time[1]).isoformat()

# print("Close test cases:")
# Example 1
# print(close_time(550, 600, arrow.get("2017-01-01T00:00:00+00:00").isoformat()))
# close_time(120, 200, arrow.now().isoformat())
# close_time(175, 200, arrow.now().isoformat())

# Example 2
# close_time(350, 600, arrow.now().isoformat())
# close_time(550, 600, arrow.now().isoformat())
# close_time(890, 600, arrow.now().isoformat())

